# Movies Sample App with Kubernetes Preview Environments

[![Develop on Okteto](https://okteto.com/develop-okteto.svg)](https://cloud.okteto.com/deploy?repository=https://gitlab.com/okteto/preview-environments)

This example shows how to leverage [Okteto](https://github.com/okteto/okteto) to develop a Node.js + React Sample App in Okteto. The Node + React Sample App is deployed using a [Helm Chart](https://github.com/okteto/movies/tree/master/chart). It creates the following components:

- A *React* based front-end, using [webpack](https://webpack.js.org) as bundler and *hot-reload server* for development.
- A  Node.js API using [Express](https://expressjs.com).
- A [MongoDB](https://www.mongodb.com) database.

The repository is configured to create a Preview Environment for every merge request automatically.